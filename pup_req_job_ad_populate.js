javascript:
/**
 * PUP Req Job Ad Pre-Populate
 * Version: 2016-10-28-pup_req_job_ad_populate-1.6b
 * Populate the PUP Position Description Job Posting Wysiwyg text area from fields above in the form.
 * Note: This is a proof of concept;
 * Branch: master
 * Branch Notes: Due to the multi-line nature of the ad. template, this version of the code isn't compatible w/ locally hosted bookmarklet. The JS needs to be un-minified to work properly.
 *
 * Usage:
 ** Authenticate to your PUP account.
 ** Navigate to manage requisitions: https://admin.dc4.pageuppeople.com/v5.3/provider/manageJobs/manageJobs.asp
 ** Open a position
 ** Click the bookmarklet
 ** bask in the pre-populated text to appear in job advertisement
 * TODO: Cross Browser Support
 ** FF PC
 ** Chrome PC
 ** Edge ?
 ** DONE - FF Mac - OK as of v. new_pd_template8-bookmarklet6 2016-08-23
 ** DONE - Chrome Mac - OK as of v. new_pd_template9-bookmarklet1 2016-08-23
 ** DONE - Safari Mac - OK as of v. new_pd_template9-bookmarklet1 2016-08-23
 * DONE - add confirm when replacing exiting text. Done as of v. new_pd_template9-bookmarklet1 2016-08-23
 * DONE - Note: PUP does not support IE; Drop support for IE. Done.
 *
 * This code is served up remotely to ensure quality source control.
 * The local Bookmarklet src:
javascript:
void(z=document.body.appendChild(document.createElement('script')));
void(z.language='javascript');
void(z.type='text/javascript');
void(z.src='https://bitbucket.org/_vid/pup_req_job_ad_populate/raw/feature-templates_by_type/pup_req_job_ad_populate.js');
 * *** Contents ***
 * Functions
 * Object mapping, capturing values of fields on the page: 'pupPdElements'
 * Create template w/ object values inserted
 * Trigger MCE code evaluation (to format code if needed)
 */

function getMceFrame(wysiwygIframeId){
  var x = document.getElementById(wysiwygIframeId);
  var y = (x.contentWindow || x.contentDocument);
  if (y.document)y = y.document;
  return y.getElementById('tinymce');
}
function replaceText(text,wysiwygIframeId) {
  var sel;
  sel = getMceFrame(wysiwygIframeId);
  if (sel) {
    if ((sel.innerHTML.length > 30 && confirm('Are you sure you want to replace the current text to appear in job advertisement?')) || sel.innerHTML.length <= 30) {
      sel.innerHTML = text;
    }
  }
}
function getTextField(elementID){
  if (document.getElementById(elementID) !== null) {
    return document.getElementById(elementID).value;
  }
  return '';
}

function getTextAreaToField(elementID){
  if (document.getElementById(elementID) !== null) {
    return document.getElementById(elementID).value.trim().replace(/(?:\r\n|\r|\n)/g, '<br />');
  }
  return '';
}

function getBooleanField(elementID){
  if (document.getElementById(elementID) !== null) {
    if (document.getElementById(elementID).checked){
      return "Yes";
    } else {
      return "No";
    }
  }
  return '';
}

/* ex: getTextField("lRoleID_fieldTitle"); */
function getSelectField(elementID){
  if (document.getElementById(elementID) !== null) {
    return document.getElementById(elementID).options[document.getElementById(elementID).selectedIndex].text;
  }
  return '';
}
/* ex getSelectField("lDepartmentID"); */
function getDropSearchField(elementID){
  result = null;
  if (typeof document.querySelectorAll('#' + elementID + ' .result-selected')[0] !== "undefined") {
    result = document.querySelectorAll('#' + elementID + ' .result-selected')[0].innerHTML;
  }
  else
    if (typeof document.querySelectorAll('#' + elementID + ' .chosen-single span')[0] !== "undefined") {
      result = document.querySelectorAll('#' + elementID + ' .chosen-single span')[0].innerHTML;
    }
  return result;
}
/* ex getDropSearchField("GenericListType_appointment_chosen"); */

function getEssentialJobDuties(){
  /* for each document.querySelectorAll('#JobDutyWrapper div.jobDuty')
  if(div.jobDuty .dutyLevel == "Essential")
  div.jobDuty .dutyPercent
  div.jobDuty .dutyDuties
  */
  dutyOutput = [];
  dutyList = document.querySelectorAll('#JobDutyWrapper div.jobDuty');
  for(i=0;i<dutyList.length;i++){
    //console.log('Pass: '+i);
    //console.log("what: " + dutyList[i].querySelectorAll('.dutyLevel')[0].innerHTML);
    //console.log(dutyList[i].querySelectorAll('.dutyLevel').length);
    if(dutyList[i].querySelectorAll('.dutyLevel')[0].innerHTML == "Essential"){
      dutyOutput.push( dutyList[i].querySelectorAll('.dutyPercent')[0].innerHTML + '%' + ' - ' + dutyList[i].querySelectorAll('.dutyDuties')[0].innerHTML);
    }
  }
  return dutyOutput.join("<br />");
}

/**
 * Object mapping, capturing values of fields on the page: 'pupPdElements'
 * Set up the data elements (tokens) to populate the wysiwyg:
 */
var pupPdElements = {
  pupPdClassTitle:    getTextField('lRoleID_fieldTitle'),
  pupPdDept:          getTextField('sOther15'),
  pupPdApptTypeDur:   getDropSearchField('GenericListType_appointment_chosen') + ', ' + getDropSearchField('GenericListType_duration_chosen'),
  pupPdFTE:           getTextField('sOther3'),
  pupPdAdvSalary:     getTextField('sOther4'),
  pupPdDeptDesc:      getTextAreaToField('sTAOther3'),
  pupPdJobDesc:       getTextAreaToField('sTAOther5'),
  pupPdReqAppMaterial:getTextAreaToField('sTAOther15'),

  pupPdEssentDuties:  '<strong>Essential Functions - Highlights</strong><b style="color:red">TODO - need Field for this (Essential Duties Narrative)</b><br /><br />' + getEssentialJobDuties(),
  pupPdMinReq:        getTextAreaToField('sTAOther4'),
  pupPdProfComp:      getTextAreaToField('sTAOther1'),
  pupPdPrefQual:      getTextAreaToField('sTAOther6'),
  pupPdSpecInstruct:  getTextAreaToField('sTAOther7'),
  pupPdAboutTheUniv:  getTextAreaToField('sTAOther8'),
  pupPdReviewBegins:  getTextField('sOther5'),
  pupPdRank:          getSelectField('GenericListType_rank'),
  pupPdFlsa:          getBooleanField('bOther10'),
  pupPdAnnualBasis:   getSelectField('lContractTypeID')
};
//rank -> GenericListType_rank
//FLSA Status -> bOther10 (radio)
//Annual Basis -> lContractTypeID (select)



/**
 * HEREDOC
 Using a non-standard method of HEREDOC in JS so we can have 'pretty' templates
 * Ref: http://stackoverflow.com/questions/805107/creating-multiline-strings-in-javascript#comment-49227719
 * Ref: http://jsfiddle.net/orwellophile/hna15vLw/2/
 */
var HEREDOC;
/* Determine which template to use based on 'Type of Position': */
switch (getSelectField('lWorkTypeID')){
  case "Classified Staff":
    HEREDOC = function EOF(){ /*!<<<EOF
    <b>Department:</b>
    <span class="Department" id="pupPdDept">${pupPdDept}</span><br />
    <b>Classification:</b>
    <span class="Title" id="pupPdClassTitle">${pupPdClassTitle}</span><br />
    <b>Appointment Type and Duration:</b>
    <span class="Duration" id="pupPdApptTypeDur">${pupPdApptTypeDur}</span><br />
    <b>Salary:</b>
    <span class="Salary" id="pupPdAdvSalary">${pupPdAdvSalary}</span><br />
    <b>FTE:</b>
    <span class="FTE" id="pupPdFTE">${pupPdFTE}</span><br />
    <p><b>Review of Applications Begins</b><br />
    <span class="Review-Begins" id="pupPdReviewBegins">${pupPdReviewBegins}</span>
    </p><p>
    <b>Special Instructions to Applicants</b><br />
    <span class="Special-Instructions" id="pupPdSpecInstruct">${pupPdSpecInstruct}</span>
    </p><p>
    <b>Department Summary</b><br />
    <span class="Department-Summary" id="pupPdDeptDesc">${pupPdDeptDesc}</span>
    </p><p>
    <b>Position Summary</b><br />
    <span class="Position-Summary" id="pupPdJobDesc">${pupPdJobDesc}</span>
    </p><p>
    <b>Minimum Requirements</b><br />
    <span class="Minimum-Requirements" id="pupPdMinReq">${pupPdMinReq}</span>
    </p><p>
    <b>Professional Competencies</b><br />
    <span class="Professional-Competencies" id="pupPdProfComp">${pupPdProfComp}</span>
    </p><p>
    <b>Preferred Qualifications</b><br />
    <span class="Preferred-Qualifications" id="pupPdPrefQual">${pupPdPrefQual}</span></p>
    <b>FLSA Exempt:</b>
    <span class="FLSA-Exempt" id="pupPdFlsa">${pupPdFlsa}</span>
    <hr /><p>
    <strong>All offers of employment are contingent upon successful completion of a background inquiry.</strong>
    </p><p class="small">
    This is a classified position represented by the SEIU Local 503, Oregon Public Employees Union.&nbsp;
    </p><p class="small">
    The University of Oregon is proud to offer a robust benefits package to eligible employees,  including health insurance, retirement plans and paid time off. For more information about benefits, visit <a href="http://hr.uoregon.edu/careers/about-benefits">http://hr.uoregon.edu/careers/about-benefits</a>.
    </p><p class="small">
    The University of Oregon is an equal opportunity, affirmative action institution committed to cultural diversity and compliance with the ADA.&nbsp; The University encourages all qualified individuals to apply, and does not discriminate on the basis of any protected status, including veteran and disability status.
    </p><p class="small">
    UO&nbsp;prohibits discrimination on the basis of race, color, sex, national or ethnic origin, age, religion,  marital status, disability, veteran status, sexual orientation, gender identity, and gender expression in all programs, activities and employment practices as required by Title IX, other applicable laws, and policies.  Retaliation is prohibited by&nbsp;UO&nbsp;policy. Questions may be referred to the Title IX Coordinator, Office of Affirmative Action and Equal Opportunity,  or to the Office for Civil Rights. Contact information, related policies, and complaint procedures are listed on the&nbsp;<a href="http://studentlife.uoregon.edu/nondiscrimination">statement of non-discrimination</a>.
    </p><p class="small">
    In compliance with federal law, the University of Oregon prepares an annual report on campus security and fire safety programs and services. The Annual Campus Security and Fire Safety Report is available online at&nbsp;<a href="http://police.uoregon.edu/annual-report">http://police.uoregon.edu/annual-report</a>.</p>
EOF
*/ }
  break;
  case "Coaches":
    HEREDOC = function EOF(){ /*!<<<EOF
    <b>Department:</b>
    <span class="Department" id="pupPdDept">${pupPdDept}</span><br />
    <b>Appointment Type and Duration:</b>
    <span class="Duration" id="pupPdApptTypeDur">${pupPdApptTypeDur}</span><br />
    <p><b>Review of Applications Begins</b><br />
    <span class="Review-Begins" id="pupPdReviewBegins">${pupPdReviewBegins}</span>
    </p><p>
    <b>Special Instructions to Applicants</b><br />
    <span class="Special-Instructions" id="pupPdSpecInstruct">${pupPdSpecInstruct}</span>
    </p><p>
    <b>Department Summary</b><br />
    <span class="Department-Summary" id="pupPdDeptDesc">${pupPdDeptDesc}</span>
    </p><p>
    <b>Position Summary</b><br />
    <span class="Position-Summary" id="pupPdJobDesc">${pupPdJobDesc}</span>
    </p><p>
    <b>Minimum Requirements</b><br />
    <span class="Minimum-Requirements" id="pupPdMinReq">${pupPdMinReq}</span>
    </p><p>
    <b>Professional Competencies</b><br />
    <span class="Professional-Competencies" id="pupPdProfComp">${pupPdProfComp}</span>
    </p><p>
    <b>Preferred Qualifications</b><br />
    <span class="Preferred-Qualifications" id="pupPdPrefQual">${pupPdPrefQual}</span></p>
    <b>FLSA Exempt:</b>
    <span class="FLSA-Exempt" id="pupPdFlsa">${pupPdFlsa}</span>
    <hr /><p>
    <strong>All offers of employment are contingent upon successful completion of a background inquiry.</strong>
    </p><p class="small">
    The University of Oregon is proud to offer a robust benefits package to eligible employees,  including health insurance, retirement plans and paid time off. For more information about benefits, visit <a href="http://hr.uoregon.edu/careers/about-benefits">http://hr.uoregon.edu/careers/about-benefits</a>.
    </p><p class="small">
    The University of Oregon is an equal opportunity, affirmative action institution committed to cultural diversity and compliance with the ADA.&nbsp; The University encourages all qualified individuals to apply, and does not discriminate on the basis of any protected status, including veteran and disability status.
    </p><p class="small">
    UO&nbsp;prohibits discrimination on the basis of race, color, sex, national or ethnic origin, age, religion,  marital status, disability, veteran status, sexual orientation, gender identity, and gender expression in all programs, activities and employment practices as required by Title IX, other applicable laws, and policies.  Retaliation is prohibited by&nbsp;UO&nbsp;policy. Questions may be referred to the Title IX Coordinator, Office of Affirmative Action and Equal Opportunity,  or to the Office for Civil Rights. Contact information, related policies, and complaint procedures are listed on the&nbsp;<a href="http://studentlife.uoregon.edu/nondiscrimination">statement of non-discrimination</a>.
    </p><p class="small">
    In compliance with federal law, the University of Oregon prepares an annual report on campus security and fire safety programs and services. The Annual Campus Security and Fire Safety Report is available online at&nbsp;<a href="http://police.uoregon.edu/annual-report">http://police.uoregon.edu/annual-report</a>.</p>
EOF
*/ }
  break;
  case "Faculty - Career":
  case "Faculty - Pro Tempore":
  case "Faculty - Other":
    HEREDOC = function EOF(){ /*!<<<EOF
    <b>Department:</b>
    <span class="Department" id="pupPdDept">${pupPdDept}</span><br />
    <b>Rank:</b>
    <span class="Rank" id="pupPdRank">${pupPdRank}</span><br />
    <b>Annual Basis:</b>
    <span class="Annual-Basis" id="pupPdAnnualBasis">${pupPdAnnualBasis}</span><br />
    <p>
    <b>Review of Applications Begins</b><br />
    <span class="Review-Begins" id="pupPdReviewBegins">${pupPdReviewBegins}</span>
    </p><p>
    <b>Special Instructions to Applicants </b><br />
    <span class="Special-Instructions" id="pupPdSpecInstruct">${pupPdSpecInstruct}</span>
    </p><p>
    <b>Department Summary</b><br />
    <span class="Department-Summary" id="pupPdDeptDesc">${pupPdDeptDesc}</span>
    </p><p>
    <b>Position Summary</b><br />
    <span class="Position-Summary" id="pupPdJobDesc">${pupPdJobDesc}</span>
    </p><p>
    <b>Minimum Requirements</b><br />
    <span class="Minimum-Requirements" id="pupPdMinReq">${pupPdMinReq}</span>
    </p><p>
    <b>Professional Competencies</b><br />
    <span class="Professional-Competencies" id="pupPdProfComp">${pupPdProfComp}</span></p>
    <b>Preferred Qualifications</b><br />
    <span class="Preferred-Qualifications" id="pupPdPrefQual">${pupPdPrefQual}</span>
    <hr /><p class="small">
    The University of Oregon is proud to offer a robust benefits package to eligible employees,  including health insurance, retirement plans and paid time off. For more information about benefits, visit <a href="http://hr.uoregon.edu/careers/about-benefits">http://hr.uoregon.edu/careers/about-benefits</a>.
    </p><p class="small">
    The University of Oregon is an equal opportunity, affirmative action institution committed to cultural diversity and compliance with the ADA.&nbsp; The University encourages all qualified individuals to apply, and does not discriminate on the basis of any protected status, including veteran and disability status.
    </p><p class="small">
    UO&nbsp;prohibits discrimination on the basis of race, color, sex, national or ethnic origin, age, religion,  marital status, disability, veteran status, sexual orientation, gender identity, and gender expression in all programs, activities and employment practices as required by Title IX, other applicable laws, and policies.  Retaliation is prohibited by&nbsp;UO&nbsp;policy. Questions may be referred to the Title IX Coordinator, Office of Affirmative Action and Equal Opportunity,  or to the Office for Civil Rights. Contact information, related policies, and complaint procedures are listed on the&nbsp;<a href="http://studentlife.uoregon.edu/nondiscrimination">statement of non-discrimination</a>.
    </p><p class="small">
    In compliance with federal law, the University of Oregon prepares an annual report on campus security and fire safety programs and services. The Annual Campus Security and Fire Safety Report is available online at&nbsp;<a href="http://police.uoregon.edu/annual-report">http://police.uoregon.edu/annual-report</a>.</p>
EOF
*/ }
  break;
  case "Faculty - Tenure Track":
    HEREDOC = function EOF(){ /*!<<<EOF
    <b>Department:</b>
    <span class="Department" id="pupPdDept">${pupPdDept}</span><br />
    <b>Rank:</b>
    <span class="Rank" id="pupPdRank">${pupPdRank}</span><br />
    <b>Annual Basis:</b>
    <span class="Annual-Basis" id="pupPdAnnualBasis">${pupPdAnnualBasis}</span><br />
    <p>
    <b>Application Deadline</b><br />
    <span class="Review-Begins" id="pupPdReviewBegins">${pupPdReviewBegins}</span>
    </p><p>
    <b>Required Application Materials</b><br />
    <span class="Req-App-Materials" id="pupPdReqAppMaterial">${pupPdReqAppMaterial}</span>
    </p><p>
    <b>Position Announcement</b><br />
    <span class="Position-Summary" id="pupPdJobDesc">${pupPdJobDesc}</span>
    </p><p>
    <b>Department or Program Summary</b><br />
    <span class="Department-Summary" id="pupPdDeptDesc">${pupPdDeptDesc}</span>
    </p><p>
    <b>Minimum Requirements</b><br />
    <span class="Minimum-Requirements" id="pupPdMinReq">${pupPdMinReq}</span>
    </p><p>
    <b>Preferred Qualifications</b><br />
    <span class="Preferred-Qualifications" id="pupPdPrefQual">${pupPdPrefQual}</span>
    <p>
    <b>About the University</b><br />
    <span class="About-the-University" id="pupPdAboutTheUniv">${pupPdAboutTheUniv}</span>
    </p>
    <hr /><p class="small">
    The University of Oregon is proud to offer a robust benefits package to eligible employees,  including health insurance, retirement plans and paid time off. For more information about benefits, visit <a href="http://hr.uoregon.edu/careers/about-benefits">http://hr.uoregon.edu/careers/about-benefits</a>.
    </p><p class="small">
    The University of Oregon is an equal opportunity, affirmative action institution committed to cultural diversity and compliance with the ADA.&nbsp; The University encourages all qualified individuals to apply, and does not discriminate on the basis of any protected status, including veteran and disability status.
    </p><p class="small">
    UO&nbsp;prohibits discrimination on the basis of race, color, sex, national or ethnic origin, age, religion,  marital status, disability, veteran status, sexual orientation, gender identity, and gender expression in all programs, activities and employment practices as required by Title IX, other applicable laws, and policies.  Retaliation is prohibited by&nbsp;UO&nbsp;policy. Questions may be referred to the Title IX Coordinator, Office of Affirmative Action and Equal Opportunity,  or to the Office for Civil Rights. Contact information, related policies, and complaint procedures are listed on the&nbsp;<a href="http://studentlife.uoregon.edu/nondiscrimination">statement of non-discrimination</a>.
    </p><p class="small">
    In compliance with federal law, the University of Oregon prepares an annual report on campus security and fire safety programs and services. The Annual Campus Security and Fire Safety Report is available online at&nbsp;<a href="http://police.uoregon.edu/annual-report">http://police.uoregon.edu/annual-report</a>.</p>
EOF
*/ }
  break;
  case "Officer of Administration":
  HEREDOC = function EOF(){ /*!<<<EOF
  <b>Department:</b>
  <span class="Department" id="pupPdDept">${pupPdDept}</span><br />
  <b>Appointment Type and Duration:</b>
  <span class="Appointment-Type-and-Duration" id="pupPdApptTypeDur">${pupPdApptTypeDur}</span><br />
  <b>Salary:</b>
  <span class="Salary" id="pupPdAdvSalary">${pupPdAdvSalary}</span><br />
  <b>FTE:</b>
  <span class="FTE" id="pupPdFTE">${pupPdFTE}</span><br />
  <p><b>Application Review Begins</b><br />
  <span class="Application-Review-Begins" id="pupPdReviewBegins">${pupPdReviewBegins}</span>
    </p><p>
    <b>Special Instructions to Applicants</b><br />
  <span class="Special-Instructions-to-Applicants" id="pupPdSpecInstruct">${pupPdSpecInstruct}</span>
    </p><p>
    <b>Department Summary</b><br />
  <span class="Department-Summary" id="pupPdDeptDesc">${pupPdDeptDesc}</span>
    </p><p>
    <b>Position Summary</b><br />
  <span class="Position-Summary" id="pupPdJobDesc">${pupPdJobDesc}</span>
    </p><p>
    <b>Minimum Requirements</b><br />
  <span class="Minimum-Requirements" id="pupPdMinReq">${pupPdMinReq}</span>
    </p><p>
    <b>Professional Competencies</b><br />
  <span class="Professional-Competencies" id="pupPdProfComp">${pupPdProfComp}</span>
    </p><p>
    <b>Preferred Qualifications</b><br />
  <span class="Preferred-Qualifications" id="pupPdPrefQual">${pupPdPrefQual}</span></p>
  <b>FLSA Exempt:</b>
  <span class="FLSA-Exempt" id="pupPdFlsa">${pupPdFlsa}</span>
    <hr /><p>
    <strong>All offers of employment are contingent upon successful completion of a background inquiry.</strong>
    </p><p class="small">
    The University of Oregon is proud to offer a robust benefits package to eligible employees,  including health insurance, retirement plans and paid time off. For more information about benefits, visit <a href="http://hr.uoregon.edu/careers/about-benefits">http://hr.uoregon.edu/careers/about-benefits</a>.
    </p><p class="small">
    The University of Oregon is an equal opportunity, affirmative action institution committed to cultural diversity and compliance with the ADA.&nbsp; The University encourages all qualified individuals to apply, and does not discriminate on the basis of any protected status, including veteran and disability status.
    </p><p class="small">
    UO&nbsp;prohibits discrimination on the basis of race, color, sex, national or ethnic origin, age, religion,  marital status, disability, veteran status, sexual orientation, gender identity, and gender expression in all programs, activities and employment practices as required by Title IX, other applicable laws, and policies.  Retaliation is prohibited by&nbsp;UO&nbsp;policy. Questions may be referred to the Title IX Coordinator, Office of Affirmative Action and Equal Opportunity,  or to the Office for Civil Rights. Contact information, related policies, and complaint procedures are listed on the&nbsp;<a href="http://studentlife.uoregon.edu/nondiscrimination">statement of non-discrimination</a>.
    </p><p class="small">
    In compliance with federal law, the University of Oregon prepares an annual report on campus security and fire safety programs and services. The Annual Campus Security and Fire Safety Report is available online at&nbsp;<a href="http://police.uoregon.edu/annual-report">http://police.uoregon.edu/annual-report</a>.</p>
EOF
*/ }
  break;
  default:
  HEREDOC = function EOF(){ /*!<<<EOF
  <strong>Default-Template</strong>
  <b>Department:</b>
  <span class="Department" id="pupPdDept">${pupPdDept}</span><br />
  <b>Classification:</b>
  <span class="Title" id="pupPdClassTitle">${pupPdClassTitle}</span><br />
  <b>Appointment Type and Duration:</b>
  <span class="Duration" id="pupPdApptTypeDur">${pupPdApptTypeDur}</span><br />
  <b>Salary:</b>
  <span class="Salary" id="pupPdAdvSalary">${pupPdAdvSalary}</span><br />
  <b>FTE:</b>
  <span class="FTE" id="pupPdFTE">${pupPdFTE}</span><br />
	<p><b>Review of Applications Begins</b><br />
  <span class="Review-Begins" id="pupPdReviewBegins">${pupPdReviewBegins}</span>
    </p><p>
    <b>Special Instructions to Applicants</b><br />
  <span class="Special-Instructions" id="pupPdSpecInstruct">${pupPdSpecInstruct}</span>
    </p><p>
    <b>Department Summary</b><br />
  <span class="Department-Summary" id="pupPdDeptDesc">${pupPdDeptDesc}</span>
    </p><p>
    <b>Position Summary</b><br />
  <span class="Position-Summary" id="pupPdJobDesc">${pupPdJobDesc}</span>
    </p><p>
    <b>Minimum Requirements</b><br />
  <span class="Minimum-Requirements" id="pupPdMinReq">${pupPdMinReq}</span>
    </p><p>
    <b>Professional Competencies</b><br />
  <span class="Professional-Competencies" id="pupPdProfComp">${pupPdProfComp}</span>
    </p><p>
    <b>Preferred Qualifications</b><br />
  <span class="Preferred-Qualifications" id="pupPdPrefQual">${pupPdPrefQual}</span></p>
  <b>FLSA Exempt:</b>
  <span class="FLSA-Exempt" id="pupPdFlsa">${pupPdFlsa}</span>
  <hr /><p>
    <strong>All offers of employment are contingent upon successful completion of a background inquiry.</strong></p>
EOF
*/ }
break;
}

adTemplate(HEREDOC.toString().split(HEREDOC.name)[2].trim()
  /* ES6 style variable interpolation looking only at pupPdElements object. Normally: this[inner]: */
  .replace(/\$\{([^}]+)\}/g,
    function(outer, inner, pos) {
      if (typeof pupPdElements[inner] === "function") {
        return pupPdElements[inner]();
      }
      return pupPdElements[inner];
   }));

function adTemplate(output) {
  replaceText(output, "sOverview_ifr");
};

/*replaceText(classifiedTemplate(), "sOverview_ifr");*/

/**
 * Trigger MCE code evaluation (to format code if needed)
 * click the code button
 * locate the open modal source code div and click() the first button Ok
 */
tinyMCE.activeEditor.buttons.code.onclick();
document.querySelectorAll('[aria-label=\'Source code\'] .mce-panel button')[0].click();
